#include "setup.h"

#ifndef ORAR
#define ORAR



class Pereche{
  private:
    string Denumire;
    string Profesor;
    string Locatie;
    short ParaImpara; // 0- Intotdeauna, 1 - Impara, 2-Para
    bool Luna;
    short ora, minuta;
  public:
    Pereche(){
      Luna = false;
      ParaImpara = 0;
    }
    Pereche& operator =(const Pereche &ob)
    {
      Denumire = ob.Denumire;
      Profesor = ob.Profesor;
      Locatie = ob.Locatie;
      ParaImpara = ob.ParaImpara;
      Luna = ob.Luna;
      ora = ob.ora;
      minuta = ob.minuta;
      return *this;
    }
    string getDenumire(){
      return Denumire;
    }
    string getProfesor(){
      return Profesor;
    }
    string getLocation(){
      return Locatie;
    }
    short getParaImpara(){
      return ParaImpara;
    }
    bool getLuna(){
      return Luna;
    }
    short getOra(){
      return ora;
    }
    short getMinuta(){
      return minuta;
    }
    void setOra(short ora, short minuta){
      this->ora = ora;
      this->minuta = minuta;
    }
    void setLocatie(string l){
      Locatie = l;
    }
    void setProfesor(string nume){
      Profesor = nume;
    }
    void setDenumire(string lectie){
      Denumire = lectie;
    }
    void setParaImpara(short ParaImpara){
      this->ParaImpara = ParaImpara;
    }
    void setLuna(bool Luna){
      this->Luna = Luna;
    }
};

class Orar{
  private:
    string ziua;
    int nrPerechi;
    Pereche *lectia;
    short id;
  public:
    Orar(string z, int nrPerechi){
      this->nrPerechi = nrPerechi;
      ziua = z;
      lectia = new Pereche[nrPerechi];
      id = 0;
    }

    void setPereche(string Denumire, string Profesor, string Locatie, short ParaImpara, short ora, short minuta, bool luna){
      if (id <= nrPerechi)
       {
        lectia[id].setDenumire(Denumire);
        lectia[id].setProfesor(Profesor);
        lectia[id].setLocatie(Locatie);
        lectia[id].setParaImpara(ParaImpara);
        lectia[id].setOra(ora, minuta);
        lectia[id].setLuna(luna);
        id++;
      }
    }

    bool getPereche(Pereche &tmp, short ora, short minuta, short saptParaImpara, short dataCurenta){
      int i;
      for (i = 0; i < nrPerechi; i++)
      {
        if (lectia[i].getOra() == ora && lectia[i].getMinuta() == minuta)
        {
          if (lectia[i].getParaImpara() == 0)
            if (!lectia[i].getLuna())
              {
                tmp = lectia[i];
                break;
              }
            else if(dataCurenta < 15)
              {
                tmp = lectia[i];
                break;
              }
          if (lectia[i].getParaImpara() == saptParaImpara){
            if (!lectia[i].getLuna())
              {
                tmp = lectia[i];
                break;
              }
            else if(dataCurenta < 15)
              {
                tmp = lectia[i];
                break;
              }
          }
        }
      }
      if (i == nrPerechi)
        return false;
      else
        return true;
    }
};

class Luna{
private:
  short *Din;       
  short *Pana;
  short *Paritate; //0 Vacanta, 1 impara, 2 Para
  short id;
public:
  Luna(int n){
    Din = new short[n];
    Pana = new short[n];
    Paritate = new short[n];
    id = -1;
  }
  ~Luna(){
    delete[] Din;
    delete[] Pana;
  }
  void setSaptamana(short Din, short Pana, short Paritate){
    id++;
    this->Din[id] = Din;
    this->Pana[id] = Pana;
    this->Paritate[id] = Paritate;
  }
  short getParitate(short data){
    for (int i = 0; i <= id; i++)
    {
      if (data >= Din[i] && data <= Pana[i])
        return Paritate[i];
    }
    return 0;
  }
};

class GraficParitate{
  private:
    Luna *noiembrie;
    Luna *decembrie;
    const short Para = 2;
    const short Impara = 1;
    short luna;
  public:
    GraficParitate(short luna){ // luna 1..12
      this->luna = luna;
      if (luna == 11)
      {
        noiembrie = new Luna(5);
        noiembrie->setSaptamana(2,7, Para);
        noiembrie->setSaptamana(9,14, Impara);
        noiembrie->setSaptamana(16,21, Para);
        noiembrie->setSaptamana(23,28, Impara);
        noiembrie->setSaptamana(30,30, Para);
      }
      if (luna == 12)
      {
        decembrie = new Luna(4);
        decembrie->setSaptamana(1,5, Para);
        decembrie->setSaptamana(7,12, Impara);
        decembrie->setSaptamana(14,19, Para);
        decembrie->setSaptamana(21,26, Impara);
      }
    }
    short getParitate(short Data)
    {
      if (luna == 11)
        return noiembrie->getParitate(Data);
      else if (luna == 12)
        return decembrie->getParitate(Data);
      return 0;
    }
};

Orar luni("luni", 6);
Orar marti("marti", 4);
Orar miercuri("miercuri", 5);
Orar joi("joi", 3);
Orar vineri("vineri", 4);
Orar sambata("sambata", 2);
GraficParitate *Calendar;

void createOrar(int zi){
  if (zi == 1){
  luni.setPereche("Seminar BD", "P. Cervac", "201", 1, 9, 45, false);
  luni.setPereche("Seminar CDE", "V. Cretu", "201", 2, 9, 45, false);
  luni.setPereche("Seminar MF", "I. Dicusara", "416", 1, 11, 30, false);
  luni.setPereche("Lab ASDN", "S. Munteanu", "217", 2, 11, 30, true);
  luni.setPereche("Lab MF", "I. Dicusara", "416", 1, 13, 30, false);
  luni.setPereche("Lab ASDN", "S. Munteanu", "217", 2, 13, 30, true);
  }
  if (zi == 1 || zi == 2){
  marti.setPereche("ASDN Curs", "V. Sudacevschi", "ONL", 2, 9, 45, false);
  marti.setPereche("ASDN Curs", "V. Sudacevschi", "ONL", 0, 11, 30, false);
  marti.setPereche("BRM ", "F. Cazac", "ONL", 0, 13, 30, false);
  marti.setPereche("BRM ", "F. Cazac", "ONL", 0, 15, 15, false);}

  if (zi == 2 || zi == 3){
  miercuri.setPereche("Lab. BRM", "F. Cazac", "Tek", 2, 8, 0, false);
  miercuri.setPereche("Lab. BRM", "F. Cazac", "Tek", 2, 9, 45, false);
  miercuri.setPereche("Lab. CDE", "V. Verbitchi", "406", 1, 11, 30, false);
  miercuri.setPereche("Sem. POO", "I. Lisnic", "224", 2, 11, 30, false);
  miercuri.setPereche("Lab. CDE", "V. Verbitchi", "406", 1, 13, 30, false);
  }
  if (zi == 3 || zi == 4){
  joi.setPereche("POO Curs", "S. Gincu", "ONL", 0, 8, 0, false);
  joi.setPereche("Baze de date", "M. Perebinos", "ONL", 0, 9, 45, false);
  joi.setPereche("MF Curs", "I. Dicusara", "ONL", 0, 8, 0, false);}
  if (zi == 4 || zi == 5){
  vineri.setPereche("Lab. POO", "I. Lisnic", "215", 0, 9, 45, false);
  vineri.setPereche("Lab. BD", "P. Cervac", "214", 1, 11, 30, true);
  vineri.setPereche("Sem. ASDN", "S. Munteanu", "224", 2, 11, 30, false);
  vineri.setPereche("Lab. BD", "P. Cervac", "214", 1, 13, 30, true);}
  if (zi == 5 || zi == 6){
  sambata.setPereche("CDE Curs", "V. Cretu", "ONL", 0, 8, 0, false);
  sambata.setPereche("CDE Curs", "V. Cretu", "ONL", 1, 9, 45, false);}
}

#endif
