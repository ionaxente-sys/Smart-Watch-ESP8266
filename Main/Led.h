#ifndef LED
#define LED
#include <FastLED.h>
#define NUM_LEDS 1
CRGB ledrgb[NUM_LEDS];

class LRGB{
  private:
    short  LedAlb;
  public:
      LRGB(short led){
        LedAlb = led;
        //pinMode(LedAlb, OUTPUT); //Dezactivez temporar
        ledrgb[0] = CRGB(0, 0, 0);
       FastLED.show();
      }
      void Light(bool);
      void SetColor(short, short, short);
};

void LRGB::SetColor(short r, short g, short b)
{
  ledrgb[0] = CRGB(r, g, b);
  FastLED.show();
}

void LRGB::Light(bool on_off)
{
  digitalWrite(LedAlb, on_off);
}

#endif
