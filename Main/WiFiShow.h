#include "setup.h"

#ifndef WIFISHOW
#define WIFISHOW

class WiFiShow{
  private:
    char ssid[32];
    
  public:
    void drawWiFi(int);
    void setSsid(char ssid[])
    {
      strcpy(this->ssid, ssid);
    }
};

void WiFiShow::drawWiFi(int SetScreen)
{
  if (SetScreen == 5)
  {
    u8g2.setDrawColor(1);
    u8g2.firstPage();
      do{
        u8g2.setFont(u8g2_font_ncenB14_tr);
        u8g2.drawStr(0,14, "Connected to:");
        u8g2.drawStr(0,30, ssid);
        u8g2.drawCircle(10,54, 6, U8G2_DRAW_ALL);
        u8g2.setFont(u8g2_font_unifont_t_symbols);
        u8g2.drawGlyph(6, 61, 0x2190); //Sageata
        u8g2.drawCircle(10,54, 8, U8G2_DRAW_ALL); //Inapoi
      }while(u8g2.nextPage());
  }
}

#endif
