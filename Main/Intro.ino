void Intro()
{
   u8g2.clearBuffer(); 
   u8g2.setDrawColor(1);
   for (int i = 1; i <= 6; i++)
   {
      u8g2.firstPage();
      do {
        u8g2.setFont(u8g2_font_VCR_OSD_tr );
          if (i == 1)
          {
            u8g2.drawCircle(64,24, 18);
            u8g2.drawFrame(20,44, 88, 4);
            u8g2.drawStr(59, 32, "A");
          }
          if (i == 2)
          {
            u8g2.drawCircle(64,24, 18);
            u8g2.drawFrame(20,44, 88, 4);
            u8g2.drawBox(20,44,17,4);
            u8g2.drawStr(59, 32, "X");
          }
          if (i == 3)
          {
            u8g2.drawCircle(64,24, 18);
            u8g2.drawFrame(20,44, 88, 4);
            u8g2.drawBox(20,44,35,4);
            u8g2.drawStr(59, 32, "E");
          }
          if (i == 4)
          {
            u8g2.drawCircle(64,24, 18);
            u8g2.drawFrame(20,44, 88, 4);
            u8g2.drawBox(20,44,52,4);
            u8g2.drawStr(59, 32, "N");
          }
          if (i == 5)
          {
            u8g2.drawCircle(64,24, 18);
            u8g2.drawFrame(20,44, 88, 4);
            u8g2.drawBox(20,44,70,4);
            u8g2.drawStr(59, 32, "T");
          }
          if (i == 6)
          {
            u8g2.drawCircle(64,24, 18);
            u8g2.drawFrame(20,44, 88, 4);
            u8g2.drawBox(20,44,88,4);
            u8g2.drawStr(59, 32, "E");
          }
          u8g2.setFont(u8g2_font_ncenB08_tr);
           u8g2.drawStr(0,57, "Powered by A. Ion  V2");
      }while (u8g2.nextPage());
      delay(833);
   }
   u8g2.clearBuffer(); 
}
