#include "setup.h"

#ifndef SETTIME
#define SETTIME
using namespace std;
class SetTimeManual{
  private:
    int hour, minuts;
    int optiune = 0;
  public:
    SetTimeManual(){
      hour = minuts = 0;
      optiune = 0;
    }
    void drawSelector(int);
    int getHour();
    int getMinuts();
    void Confirm();
    int getOption(){
      return optiune;
    }
    void increment();
    void decrement();
    void resetop(){
      optiune=0;
    }
};

void SetTimeManual::increment()
{
  if (optiune == 1)
    hour++;
  if (optiune == 2)
    minuts++;
  if (hour == 24)
    hour = 0;
  if (minuts == 60)
    minuts = 0;
}

void SetTimeManual::decrement()
{
  if (optiune == 1)
    hour--;
  if (optiune == 2)
    minuts--;
  if (hour == -1)
    hour = 23;
  if (minuts == -1)
    minuts = 59;
}


void SetTimeManual::Confirm()
{
  optiune++;
  if (optiune == 4)
    optiune = 0;
}


int SetTimeManual::getHour(){
  return hour;
}

int SetTimeManual::getMinuts(){
  return minuts;
}

void SetTimeManual::drawSelector(int SetScreen)
{
  String h=String(hour);
  String m=String(minuts);
  if (SetScreen == 6){
     u8g2.setDrawColor(1);
      u8g2.firstPage();
      do{
        u8g2.setFont(u8g2_font_ncenB14_tr);
        u8g2.drawStr(0,30, h.c_str());
        u8g2.drawStr(20,30,":");
        u8g2.drawStr(24,30, m.c_str());
        }while(u8g2.nextPage());
   }
}

#endif
