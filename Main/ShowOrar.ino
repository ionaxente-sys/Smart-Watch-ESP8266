void drawPerechi(short weekDay, short day)
{
  Pereche *temporar = new Pereche;
  int Linie = -1;
  int Linie2 = -1;
  int Rand[4] = {23, 36, 49, 62};
  short ora[4] = {8,9,11,13};
  short minute[4] = {0, 45, 30, 30};
  if (weekDay == 1)
    for (int i=0; i <= 4; i++){
      if (luni.getPereche(*temporar, ora[i], minute[i], Calendar->getParitate(day), day))
      {
        Linie++;
        u8g2.drawStr(0,Rand[Linie], temporar->getDenumire().c_str());
        u8g2.drawStr(49 ,Rand[Linie], temporar->getLocation().c_str());
      }
      if (marti.getPereche(*temporar, ora[i], minute[i], Calendar->getParitate(day), day))
      {
        Linie2++;
        u8g2.drawStr(66,Rand[Linie2], temporar->getDenumire().c_str());
        u8g2.drawStr(114 ,Rand[Linie2], temporar->getLocation().c_str());
      }
    }
  
   if (weekDay == 2)
    for (int i=0; i <= 4; i++){
      if (marti.getPereche(*temporar, ora[i], minute[i], Calendar->getParitate(day), day))
      {
        Linie++;
        u8g2.drawStr(0,Rand[Linie], temporar->getDenumire().c_str());
        u8g2.drawStr(114,Rand[Linie], temporar->getLocation().c_str());
      }
      if (miercuri.getPereche(*temporar, ora[i], minute[i], Calendar->getParitate(day), day))
      {
        Linie2++;
        u8g2.drawStr(66,Rand[Linie2], temporar->getDenumire().c_str());
        u8g2.drawStr(114,Rand[Linie2], temporar->getLocation().c_str());
      }
    }
   if (weekDay == 3)
    for (int i=0; i <= 4; i++){
      if (miercuri.getPereche(*temporar, ora[i], minute[i], Calendar->getParitate(day), day))
      {
        Linie++;
        u8g2.drawStr(0,Rand[Linie], temporar->getDenumire().c_str());
        u8g2.drawStr(49 ,Rand[Linie], temporar->getLocation().c_str());
      }
      if (joi.getPereche(*temporar, ora[i], minute[i], Calendar->getParitate(day), day))
      {
        Linie2++;
        u8g2.drawStr(66,Rand[Linie2], temporar->getDenumire().c_str());
        u8g2.drawStr(114 ,Rand[Linie2], temporar->getLocation().c_str());
      }
    }
   if (weekDay == 4)
    for (int i=0; i <= 4; i++){
      if (joi.getPereche(*temporar, ora[i], minute[i], Calendar->getParitate(day), day))
      {
        Linie++;
        u8g2.drawStr(0,Rand[Linie], temporar->getDenumire().c_str());
        u8g2.drawStr(49 ,Rand[Linie], temporar->getLocation().c_str());
      }
      if (vineri.getPereche(*temporar, ora[i], minute[i], Calendar->getParitate(day), day))
      {
        Linie2++;
        u8g2.drawStr(66,Rand[Linie2], temporar->getDenumire().c_str());
        u8g2.drawStr(114 ,Rand[Linie2], temporar->getLocation().c_str());
      }
    }
   if (weekDay == 5)
    for (int i=0; i <= 4; i++){
      if (vineri.getPereche(*temporar, ora[i], minute[i], Calendar->getParitate(day), day))
      {
        Linie++;
        u8g2.drawStr(0,Rand[Linie], temporar->getDenumire().c_str());
        u8g2.drawStr(49 ,Rand[Linie], temporar->getLocation().c_str());
      }
      if (sambata.getPereche(*temporar, ora[i], minute[i], Calendar->getParitate(day), day))
      {
        Linie2++;
        u8g2.drawStr(66,Rand[Linie2], temporar->getDenumire().c_str());
        u8g2.drawStr(114 ,Rand[Linie2], temporar->getLocation().c_str());
      }
    }
  if (weekDay == 6)
    for (int i=0; i <= 4; i++){
      if (sambata.getPereche(*temporar, ora[i], minute[i], Calendar->getParitate(day), day))
      {
        Linie++;
        u8g2.drawStr(0,Rand[Linie], temporar->getDenumire().c_str());
        u8g2.drawStr(49 ,Rand[Linie], temporar->getLocation().c_str());
      }
      u8g2.setBitmapMode(1);
      u8g2.drawXBM(66, 10, Holiday_width, Holiday_height, Holiday_bits);
     }
  if (weekDay == 7)
  {
    u8g2.setBitmapMode(1);
    u8g2.drawXBM(5, 10, Holiday_width, Holiday_height, Holiday_bits);
    for (int i=0; i <= 4; i++){
      if (luni.getPereche(*temporar, ora[i], minute[i], Calendar->getParitate(day), day))
      {
        Linie2++;
        u8g2.drawStr(66,Rand[Linie2], temporar->getDenumire().c_str());
        u8g2.drawStr(114,Rand[Linie2], temporar->getLocation().c_str());
      }
    }
  }
  delete temporar;
}

void drawOrar(int SetScreen, short weekDay, short Day)
{  
  if (SetScreen == 3)
  {    
    int i;    
    u8g2.setDrawColor(1);
    u8g2.firstPage();
      do{
          for (i = 0; i <= 128; i+= 8)
          {
            u8g2.drawHLine(i, 11, 4);
            if (i <= 64)
              u8g2.drawVLine(64, i, 4);
          }
          u8g2.setFont(u8g2_font_ncenB10_tr);
          u8g2.drawStr(22,10, "Azi");
          u8g2.drawStr(72,10, "Maine");
          u8g2.setFont(u8g2_font_5x8_tr);
          drawPerechi(weekDay, Day);
      }while(u8g2.nextPage());
  }
}
