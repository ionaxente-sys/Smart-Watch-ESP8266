#ifndef WATCH
#define WATCH
class WatchActualizer{
  private:
    int hour;
    int minuts;
    int seconds;
    unsigned long int interval;
    unsigned long int timp, timp_trecut;
    int compensatie;
    int Data, Month, WeekDay;
  public:
    WatchActualizer(){
      Data = Month = WeekDay = 0;
      hour = minuts = seconds = 0;
      timp = millis();
      timp_trecut = 0;
      interval = 1000;
      compensatie = 0;
    }
    void Go(bool);
    void setSeconds(int);
    void setMinuts(int);
    void setHour(int);
    int getSeconds();
    int getMinuts();
    int getHour();
    void setDate(int, int, int);
    int getDay(){
      return Data;
    }
    int getMonth(){
      return Month;
    }
    int getWeekDay(){
      return WeekDay;
    }
};

void WatchActualizer::setDate(int Data, int Month, int WeekDay)
{
  this->Data = Data;
  this->Month = Month;
  this->WeekDay = WeekDay;
}

int WatchActualizer::getSeconds()
{
  return seconds;
}

int WatchActualizer::getMinuts()
{
  return minuts;
}

int WatchActualizer::getHour()
{
  return hour;
}

void WatchActualizer::setSeconds(int s)
{
  seconds = s;
}
void WatchActualizer::setMinuts(int m)
{
  minuts = m;
}
void WatchActualizer::setHour(int h)
{
  hour = h;
}

void WatchActualizer::Go(bool debug)
{
  timp = millis();
  if (timp - timp_trecut >= interval)
  {
    compensatie += timp-timp_trecut-interval;
    if (debug) Serial.printf("Compensatie: %d\n", compensatie);
    seconds++;
    if (compensatie >= interval)
    {
      seconds++;
      compensatie = 0;
    }
    timp_trecut=timp;
  }
  if (seconds == 60)
  {
    seconds = 0;
    minuts++;
  }
  if (minuts == 60)
  {
    minuts = 0;
    hour++;
  }
  if (hour == 24)
    hour = 0;
}

#endif
