void ControlButtons(){
  int option;
  bool pressed = false;
  if (butoane.pressed_any())
  {
    Serial.printf("SetScreen: %d", SetScreen);
    if (SetScreen == 2)
    {
      if (butoane.pressed(Down))
        menu.increment();
      if (butoane.pressed(Up))
        menu.decrement();
      if (butoane.pressed(Select)){
        pressed = true;
        option = menu.getOption();
        if(option == 0)
            SetScreen = 1;
        if (option == 1)
            SetScreen = 3;
        if (option == 3)
            SetScreen = 5;
        if (option == 4)
          SetScreen = 6;
      }
    }

    if (SetScreen == 3 && !pressed)
    {
      if (butoane.pressed(Select))
      {
        SetScreen = 2;
      }
    }
    
    if (SetScreen == 5 && !pressed)
    {
      if (butoane.pressed(Select))
      {
        SetScreen = 2;
      }
    }

    if (SetScreen == 7)
    {
      if (butoane.pressed_any())
      {
        Notificare.deactivate();
        SetScreen = 1;
      }
    }
    
    if(SetScreen == 6)
    {
      if (butoane.pressed(Up))
        SetTimeM.increment();
      if (butoane.pressed(Down))
        SetTimeM.decrement();
      if (SetTimeM.getOption() == 3)
      {
        
        SetTimeM.resetop();
        timpCurent.setHour(SetTimeM.getHour());
        timpCurent.setMinuts(SetTimeM.getMinuts());
        SetScreen = 2;
      }
      if (butoane.pressed(Select))
        SetTimeM.Confirm();
        
    }

    if (SetScreen == 1)
      if (butoane.pressed(Up))
        SetScreen++;
    
    if (butoane.pressed(buttonPowerSave))
    {
        if (!PowerSaveMode)
          PowerSaveMode = !PowerSaveMode;
        else
        {
          PowerSaveMode = !PowerSaveMode;
        }
        u8g2.setPowerSave(PowerSaveMode);
    }

    //Saturare buton
    if (SetScreen > maxScreens)
      SetScreen = maxScreens;
    if (SetScreen < 1)
      SetScreen = 1;
    //evitare intarziere
    while (butoane.pressed_any()){
      timpCurent.Go(debug);
     }
  }  
}
