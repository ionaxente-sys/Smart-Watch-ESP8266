#ifndef BUTTONS
#define BUTTONS
class Buttons{
  private:
    int up;
    int down;
    int select;
    int PowerSave;
  public:
    Buttons(int ,int , int, int );
    bool pressed(int );
    bool pressed_any();
};

Buttons::Buttons(int up, int down, int select, int PowerSave)
{
  this->up = up; this->down = down; this->select = select; this->PowerSave = PowerSave;
  pinMode(up, INPUT_PULLUP);
  pinMode(down, INPUT_PULLUP);
  pinMode(select, INPUT_PULLUP);
  pinMode(PowerSave, INPUT);
}

bool Buttons::pressed(int pin){
  return !digitalRead(pin);
}

bool Buttons::pressed_any()
{
  return pressed(up) || pressed(down) || pressed(select) || pressed(PowerSave);
}

#endif
