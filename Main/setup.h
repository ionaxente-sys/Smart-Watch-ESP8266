#ifndef SETUP_H
#define SETUP_H

//Include bibliotecile de care avem nevoie
#include <U8g2lib.h>
#include <ESP8266WiFi.h>
#include <ESP8266HTTPClient.h>
#include <String>
#ifdef U8X8_HAVE_HW_I2C
#include <Wire.h>
#endif
U8G2_SH1106_128X64_NONAME_1_HW_I2C u8g2(U8G2_R0, /* reset=*/ U8X8_PIN_NONE, 4, 5); //Setare Ecran
#include "Buttons.h"
#include "Led.h"
#include "Watch.h"
#include "Colect_data.h"
#include "Menu.h"
#include "FirstScreen.h"
#include "SetTimeManual.h"
#include "WiFiShow.h"
#include "OrarUtm.h"


//Declararea Pinilor I/O
#define Up 12
#define Down 13
#define Select 14
#define HighLightLed 16
#define LedRgb 15
#define buttonPowerSave 2

//Debug variabila
bool debug = false;
bool Succes_connected;
int ora;

//Declararea obiectelor
Buttons butoane(Up, Down, Select, buttonPowerSave); //Butoane
LRGB LedNotificare(HighLightLed); //Ledurile notificatoare
WatchActualizer timpCurent; //cronometru
FirstScreen Watch; //Ceasul
ColectData downloadData(debug);
Menu menu;
SetTimeManual SetTimeM;
WiFiShow WiFiScreen;

void drawSignal(int Semnal);

#include "Notification.h"

Notification Notificare;



//Interfata ecrane
int SetScreen = 1;
int maxScreens = 7;
bool PowerSaveMode = false;

#endif
