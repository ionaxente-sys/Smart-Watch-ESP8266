#include "setup.h"
#include  "WIFI_ICONS.h"
#ifndef FIRSTSCREEN
#define FIRSTSCREEN
class FirstScreen{
  private:
    double grade_secunde;
    double grade_minute;
    double grade_ora;
    int raza_cerculete;
    int raza_ceas;
    int x,y;
    int x_minute, y_minute;
    int x_secunde, y_secunde;
  public:
     FirstScreen(){
      raza_cerculete = 2;
      raza_ceas = 30;
     }
     void drawWatch(int, int, int, int);
      void drawSignal(int Semnal)
      {
        u8g2.setBitmapMode(1);
        if (Semnal <= 0 && Semnal >= -30)
           u8g2.drawXBM(98, 0, WIFI_100_width, WIFI_100_height, WIFI_100_bits);
        if (Semnal <= -31 && Semnal >= -50)
           u8g2.drawXBM(98, 0, WIFI_75_width, WIFI_75_height, WIFI_75_bits);
        if (Semnal <= -50 && Semnal >= -66)
           u8g2.drawXBM(98, 0, WIFI25_width, WIFI25_height, WIFI25_bits);
        if (Semnal <= -67)
           u8g2.drawXBM(98, 0, WIFI_0_width,WIFI_0_height, WIFI_0_bits);
     }
};

void FirstScreen::drawWatch(int ora, int minute, int secunde, int SetScreen)
{
  if (SetScreen == 1){
      //Conversie in grade ora, minute, secunde
      grade_secunde = secunde*6;
      grade_minute = minute*6;
      grade_ora = (ora*60+minute)/2;
    
      if ((grade_ora-360) >= 0)
        grade_ora -= 360;
    
      //Obtinere x_y ora
      x = raza_ceas*sin(grade_ora*PI/180.0)+64;
      y = 64-(raza_ceas*cos(grade_ora*PI/180.0)+32);
    
      //Obtinere x_y minute
      x_minute = raza_ceas*sin(grade_minute*PI/180.0)+64;
      y_minute = 64-(raza_ceas*cos(grade_minute*PI/180.0)+32);
    
      //Obtinere x_y minute
      x_secunde = raza_ceas*sin(grade_secunde*PI/180.0)+64;
      y_secunde = 64-(raza_ceas*cos(grade_secunde*PI/180.0)+32);
    
      //Desenare
      u8g2.setDrawColor(1);
      u8g2.firstPage();
      do {
        drawSignal(WiFi.RSSI());
        u8g2.drawCircle(64, 2, raza_cerculete, U8G2_DRAW_ALL);// 12
        u8g2.drawCircle(79, 5, raza_cerculete, U8G2_DRAW_ALL);// 1
        u8g2.drawCircle(90, 16, raza_cerculete, U8G2_DRAW_ALL);// 2
        u8g2.drawCircle(95, 32, raza_cerculete, U8G2_DRAW_ALL);// 3
        u8g2.drawCircle(90, 47, raza_cerculete, U8G2_DRAW_ALL);// 4
        u8g2.drawCircle(79, 59, raza_cerculete, U8G2_DRAW_ALL);// 5
        u8g2.drawCircle(64, 61, raza_cerculete, U8G2_DRAW_ALL);// 6
        u8g2.drawCircle(48, 59, raza_cerculete, U8G2_DRAW_ALL);// 7
        u8g2.drawCircle(37, 47, raza_cerculete, U8G2_DRAW_ALL);// 8
        u8g2.drawCircle(33, 32, raza_cerculete, U8G2_DRAW_ALL);// 9
        u8g2.drawCircle(37, 16, raza_cerculete, U8G2_DRAW_ALL);// 10
        u8g2.drawCircle(48, 5, raza_cerculete, U8G2_DRAW_ALL);// 11
        u8g2.drawCircle(x, y, raza_cerculete+3, U8G2_DRAW_ALL);// Ora
        u8g2.drawCircle(x_minute, y_minute, raza_cerculete+2, U8G2_DRAW_ALL);// Minute
        u8g2.drawCircle(x_secunde, y_secunde, raza_cerculete+1, U8G2_DRAW_ALL);// Secunde
        u8g2.drawTriangle(x,y, x_minute, y_minute, x_secunde, y_secunde); //Triunghi umplut
        
      } while ( u8g2.nextPage() );
  }
}

#endif
