#include "setup.h"

#ifndef MENU
#define MENU

class Menu{
  private:
    int optiune;
    int distanta, raza;
  public:
    Menu(){
      optiune = 0;
      distanta = 22;
      raza = 10;
    }
    void DrawMenu(int SetScreen);
    void increment();
    void decrement();
    int getOption();
};

int Menu::getOption(){
  return optiune;
}

void Menu::increment(){
  optiune++;
  if (optiune > 4)
    optiune = 0;
}

void Menu::decrement(){
  optiune--;
  if (optiune < 0)
    optiune = 4;
}

void Menu::DrawMenu(int SetScreen){
   if (SetScreen == 2){
      u8g2.setDrawColor(1);
      u8g2.firstPage();
      do{
        if (optiune == 0)
          u8g2.drawCircle(10,54, 8, U8G2_DRAW_ALL); //Inapoi
        if (optiune == 1)
          u8g2.drawCircle(64,30+distanta, raza+2, U8G2_DRAW_ALL); //Jos
        if (optiune == 2)
          u8g2.drawCircle(64+distanta,32, raza+2, U8G2_DRAW_ALL); //Dreappta
        if (optiune == 3)
          u8g2.drawCircle(64,32-distanta, raza+2, U8G2_DRAW_ALL); // Sus
        if (optiune == 4)
          u8g2.drawCircle(64-distanta,32, raza+2, U8G2_DRAW_ALL); //Stanga
          
        u8g2.setFont(u8g2_font_unifont_t_symbols);
        u8g2.drawCircle(64,32-distanta, raza, U8G2_DRAW_ALL); // Sus
        u8g2.drawCircle(64,30+distanta, raza, U8G2_DRAW_ALL); //Jos
        u8g2.drawCircle(64-distanta,32, raza, U8G2_DRAW_ALL); //Stanga
        u8g2.drawCircle(64+distanta,32, raza, U8G2_DRAW_ALL); //Dreappta
        u8g2.drawGlyph(35, 38, 0x23f0); //Ceasul
        u8g2.drawGlyph(60, 16, 0x2600); //WiFi
        u8g2.drawGlyph(79, 38, 0x2615); //Terminal Debug
        u8g2.drawGlyph(60, 58, 0x25EB); //Orar pe 2 zile
        u8g2.drawCircle(10,54, 6, U8G2_DRAW_ALL);
        u8g2.drawGlyph(6, 61, 0x2190); //Sageata
      }while(u8g2.nextPage());
   }
}

#endif
