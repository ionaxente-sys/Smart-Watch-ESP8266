#include "setup.h"

#ifndef NOTIFICATION
#define NOTIFICATION

#define Notification_width 36
#define Notification_height 36
static unsigned char Notification_bits[] = {
  0x00, 0x00, 0x0F, 0x00, 0x00, 0x00, 0x80, 0x19, 0x00, 0x00, 0x00, 0xC0, 
  0x30, 0x18, 0x00, 0x00, 0x70, 0x60, 0xFF, 0x00, 0x00, 0x0C, 0x80, 0xE7, 
  0x01, 0x00, 0x06, 0x80, 0xE3, 0x01, 0x00, 0x03, 0x80, 0xEF, 0x03, 0x00, 
  0x01, 0xC0, 0xE7, 0x01, 0x80, 0x01, 0x80, 0xE7, 0x01, 0x80, 0x00, 0x00, 
  0xFF, 0x00, 0x80, 0x00, 0x00, 0x7E, 0x00, 0x80, 0x01, 0x00, 0x00, 0x00, 
  0x80, 0x00, 0x00, 0x00, 0x00, 0x80, 0x01, 0x00, 0x18, 0x00, 0x80, 0x00, 
  0x00, 0x10, 0x00, 0x80, 0x01, 0x00, 0x18, 0x00, 0x80, 0x00, 0x00, 0x10, 
  0x00, 0x80, 0x01, 0x00, 0x18, 0x00, 0x80, 0x00, 0x00, 0x30, 0x00, 0xC0, 
  0x00, 0x00, 0x30, 0x00, 0x60, 0x00, 0x00, 0xC0, 0x00, 0x10, 0x00, 0x00, 
  0x80, 0x00, 0x18, 0x00, 0x00, 0x80, 0x01, 0x0C, 0x00, 0x00, 0x00, 0x03, 
  0x04, 0x00, 0x00, 0x00, 0x02, 0x06, 0x00, 0x00, 0x00, 0x06, 0x02, 0x00, 
  0x00, 0x00, 0x04, 0x03, 0x00, 0x00, 0x00, 0x0C, 0xFF, 0xFF, 0xFF, 0xFF, 
  0x0F, 0x80, 0x4A, 0xA2, 0x82, 0x04, 0x00, 0xE0, 0xFF, 0x00, 0x00, 0x00, 
  0x60, 0x40, 0x00, 0x00, 0x00, 0x60, 0x40, 0x00, 0x00, 0x00, 0x40, 0x20, 
  0x00, 0x00, 0x00, 0xC0, 0x39, 0x00, 0x00, 0x00, 0x00, 0x1F, 0x00, 0x00, 
  };
  
class Notification{
  private: 
    bool activate;
    short ora[4] = {8,9,11,13};
    short minute[4] = {0, 45, 30, 30};
    short preintampinare;
    Pereche *temp;
    int color;
    bool sens;
    bool sen1;
    float brightness;
    long prev_time;
  public:
    Notification(){
      activate = false;
      temp = new Pereche;
      preintampinare = 5; //Minute
      brightness = 0.0;
      prev_time = millis();
      sens = true;
      sen1 = true;
      color = 0; 
    }
    void deactivate()
    {
      activate = false;
      LedNotificare.SetColor(0,0,0);
    }
    void drawNotification(int);
    void blynk_color();
    bool verficaDacaAmLectie(short hour, short minuts, short weekDay, short day, int SetScreen)
    {
      int i;
      if (activate)
      {
        drawNotification(SetScreen);
        return true;
      }
       LedNotificare.SetColor(0,0,0);
      for (i = 0; i < 4; i++)
      {
        if (ora[i] == hour){
          if (minuts+preintampinare == minute[i] || (minuts+preintampinare == 60 && minute[i] == 0))
          {
             if (weekDay == 1){
              if (luni.getPereche(*temp, ora[i], minute[i], Calendar->getParitate(day), day))
                {
                  activate = true;
                  return activate;
                }}
             else if (weekDay == 2){
              if (marti.getPereche(*temp, ora[i], minute[i], Calendar->getParitate(day), day))
                {
                  activate = true;
                  return activate;
                }}
             else if (weekDay == 3){
              if (miercuri.getPereche(*temp, ora[i], minute[i], Calendar->getParitate(day), day))
                {
                  activate = true;
                  return activate;
                }}
             else if (weekDay == 4){
              if (joi.getPereche(*temp, ora[i], minute[i], Calendar->getParitate(day), day))
                {
                  activate = true;
                  return activate;
                }}
             else if (weekDay == 5){
              if (vineri.getPereche(*temp, ora[i], minute[i], Calendar->getParitate(day), day))
                {
                  activate = true;
                  return activate;
                }}
              else if (weekDay == 6){
                if (sambata.getPereche(*temp, ora[i], minute[i], Calendar->getParitate(day), day))
                {
                  activate = true;
                  return activate;
                }
              }
          }
          }
      }
      return false;
    }
};

void Notification::blynk_color()
{
    LedNotificare.SetColor((255-color)/brightness, (color)/brightness, (255-color)/brightness);
    while  (millis()-prev_time <= 20){timpCurent.Go(false);}
    if (sen1)
    {
      brightness += 0.01;
    }
    if (!sen1)
    {
      brightness -= 0.01;
    }
    if (brightness == 1 || brightness == 0)
      sen1 = !sen1;
    
    if (sens)
      color++;
    if (color == 256)
      sens = !sens;
    if (!sens)
      color--;
    if (color == 0)
      sens = !sens;
}

void Notification::drawNotification(int SetScreen){
  if (activate && SetScreen == 7)
  { 
    blynk_color();
    u8g2.setDrawColor(1);
      u8g2.firstPage();
      do {
        u8g2.setBitmapMode(1);
        u8g2.drawXBM(0, 0, Notification_width, Notification_height, Notification_bits);
        u8g2.setFont(u8g2_font_ncenB10_tr);
        u8g2.drawStr(36,20, temp->getDenumire().c_str());
        u8g2.drawStr(68,40, temp->getLocation().c_str());
        u8g2.setFont(u8g2_font_5x8_tr);
        u8g2.drawStr(0,58, "Perechea incepe peste");
        u8g2.drawStr(60,64, "5 min !");
      } while ( u8g2.nextPage());
  }
}

#endif
