#define WIFI_DELAY        500
/* Max SSID octets. */
#define MAX_SSID_LEN      32
/* Wait this much until device gets IP. */
#define MAX_CONNECT_TIME  30000

/* SSID that to be stored to connect. */
char ssid[MAX_SSID_LEN] = "";

void scanAndSort() {
  memset(ssid, 0, MAX_SSID_LEN);
  int n = WiFi.scanNetworks();
  if (debug) Serial.println("Scan complete!");
  if (n == 0) {
    if (debug) Serial.println("No networks available.");
  } else {
    if (debug) Serial.print(n);
    if (debug) Serial.println(" networks discovered.");
    int indices[n];
    for (int i = 0; i < n; i++) {
      indices[i] = i;
    }
    for (int i = 0; i < n; i++) {
      for (int j = i + 1; j < n; j++) {
        if (WiFi.RSSI(indices[j]) > WiFi.RSSI(indices[i])) {
          std::swap(indices[i], indices[j]);
        }
      }
    }
    for (int i = 0; i < n; ++i) {
      if (debug) Serial.println("The strongest open network is:");
      if (debug) Serial.print(WiFi.SSID(indices[i]));
      if (debug) Serial.print(" ");
      if (debug) Serial.print(WiFi.RSSI(indices[i]));
      if (debug) Serial.print(" ");
      if (debug) Serial.print(WiFi.encryptionType(indices[i]));
      if (debug) Serial.println();
      if(WiFi.encryptionType(indices[i]) == ENC_TYPE_NONE) {
        memset(ssid, 0, MAX_SSID_LEN);
        strncpy(ssid, WiFi.SSID(indices[i]).c_str(), MAX_SSID_LEN);
        break;
      }
    }
  }
}



void Initialize()
{
  u8g2.begin();
  if (debug)
    Serial.begin(115200);
  //Intializare Led
  FastLED.addLeds<WS2812, LedRgb, GRB>(ledrgb, NUM_LEDS);
  //---------------

  
  if (debug) Serial.println("Scanning for open networks...");
   if(WiFi.status() != WL_CONNECTED) {
    /* Clear previous modes. */
    WiFi.softAPdisconnect();
    WiFi.disconnect();
    WiFi.mode(WIFI_STA);
    delay(WIFI_DELAY);
    /* Scan for networks to find open guy. */
    scanAndSort();
    delay(WIFI_DELAY);
    /* Global ssid param need to be filled to connect. */
    if(strlen(ssid) > 0) {
      if (debug) Serial.print("Connecting to ");
      if (debug) Serial.println(ssid);
      /* No pass for WiFi. We are looking for non-encrypteds. */
      WiFi.begin(ssid);
      unsigned short try_cnt = 0;
      /* Wait until WiFi connection but do not exceed MAX_CONNECT_TIME */
      while (WiFi.status() != WL_CONNECTED && try_cnt < MAX_CONNECT_TIME / WIFI_DELAY) {
        delay(WIFI_DELAY);
        if (debug) Serial.print(".");
        try_cnt++;
      }
      if(WiFi.status() == WL_CONNECTED) {
        WiFiScreen.setSsid(ssid);
        if (debug) Serial.println("");
        if (debug) Serial.println("Connection Successful!");
        if (debug) Serial.println("Your device IP address is ");
        if (debug) Serial.println(WiFi.localIP());
        Succes_connected = true;
      } else {
        if (debug) Serial.println("Connection FAILED");
      }
    } else {
      Succes_connected = false;
      if (debug) Serial.println("No open networks available. :-(");  
    }
  }
  
  
}
