#include "setup.h"

WiFiClient client;
HTTPClient http;

#ifndef COLECTDATA
#define COLECTDATA

  class ColectData{
    private:
      const char* host = "api.thingspeak.com";
      const int httpPortRead = 80;
      String Date_colectate;
      short hour, minuts, seconds;
      unsigned long timp_connect, timp_connect_passed;
      bool debug;
    public:
      ColectData(){
        hour = minuts = seconds = 0;
        timp_connect_passed = 0;
        debug = false;
      }
      ColectData(bool debug){
        hour = minuts = seconds = 0;
        timp_connect_passed = 0;
        this->debug = debug;
      }
      bool strFind(String strfind, String source)
      {
        int j = 0;
        for (int i= 0; i <= source.length(); i++)
        {
          while (strfind[j] == source[i+j])
          {
            if (j == strfind.length()-1)
            {
              return true;
            }
            j++;
          }
          j = 0;
        }
        return false;
      }
      void colect_data(char *);
      int getHour();
      int getMinuts();
      int getSeconds();
      int getDay();
      int getMonth();
      int getWeekDay();
  };

int ColectData::getDay()
{
  bool start = false;
  int day = 0;
  for (int i=0; i <= Date_colectate.length(); i++)
  {
    if (Date_colectate[i] >= '0' && Date_colectate[i] <= '9')
      start = true;
    if (Date_colectate[i] == ',' && start){
      start = false;
      return day;
    }
    if (start){
      day = day*10+(Date_colectate[i] - '0');
    }
  }
  return 0;
}

  int ColectData::getMonth(){
    short day;
    if (strFind("Jan", Date_colectate))
      day = 1;
    else if (strFind("Feb", Date_colectate))
      day = 2;
    else if (strFind("Mar", Date_colectate))
      day = 3;
    else if (strFind("Apr", Date_colectate))
      day = 4;
    else if (strFind("May", Date_colectate))
      day = 5;
    else if (strFind("Jun", Date_colectate))
      day = 6;
    else if (strFind("Jul", Date_colectate))
      day = 7;
    else if (strFind("Aug", Date_colectate))
      day = 8;
    else if (strFind("Sep", Date_colectate))
      day = 9;
    else if (strFind("Oct", Date_colectate))
      day = 10;
    else if (strFind("Nov", Date_colectate))
      day = 11;
    else if (strFind("Dec", Date_colectate))
      day = 12;
    return day;
  }

  int ColectData::getWeekDay()
  {
    short day;
  
     if (strFind("Mon", Date_colectate))
        day = 1;
      else if (strFind("Tue", Date_colectate))
        day = 2;
      else if (strFind("Wed", Date_colectate))
        day = 3;
      else if (strFind("Thu", Date_colectate))
        day = 4;
      else if (strFind("Fri", Date_colectate))
        day = 5;
      else if (strFind("Sat", Date_colectate))
        day = 6;
      else if (strFind("Sun", Date_colectate))
        day = 7;
      return day;
  }

  int ColectData::getHour(){
    const char *cuv = Date_colectate.c_str();
    int ora= 0;
    int i = 0, cont = 0;
    while (cuv[i] != '\0')
    {
       if (cuv[i] >= '0' && cuv[i] <= '9')
          {
            ora = ora*10+cuv[i]-48;
          }
       if (cuv[i] == ':'){
          break;
       }
          i++;
    }
    return ora; 
  }

  int ColectData::getMinuts(){
    const char *cuv = Date_colectate.c_str();
    int minute= 0;
    int i = 0, cont = 0;
    while (cuv[i] != ':')
      i++;
    i++;
    while (cuv[i] != '\0')
    {
       if (cuv[i] >= '0' && cuv[i] <= '9')
          {
            minute = minute*10+cuv[i]-48;
          }
       if (cuv[i] == ':'){
         break;
       }
          i++;
    }
    return minute;
  }

  int ColectData::getSeconds(){
    const char *cuv = Date_colectate.c_str();
    int secunde= 0;
    int i = 0, cont = 0;
    while (cuv[i] != ':')
      i++;
    i++;
    while (cuv[i] != ':')
      i++;
    i++;
    while (cuv[i] != '\0')
    {
       if (cuv[i] >= '0' && cuv[i] <= '9')
          {
            secunde = secunde*10+cuv[i]-48;
          }
       if (cuv[i] == ':'){
         break;
       }
          i++;
    }
    return secunde;
  }

  void ColectData::colect_data(char *url)
  {
    timp_connect = millis();
    if (timp_connect- timp_connect_passed >= 5000)
    {
      if (http.begin(host, httpPortRead, url))
       {
          int httpCode = http.GET();
          if (httpCode > 0)
          {
              if (httpCode == HTTP_CODE_OK || httpCode == HTTP_CODE_MOVED_PERMANENTLY)
              {
                Date_colectate = http.getString();
                if (debug) Serial.printf("Date colectate: %s\n", Date_colectate.c_str());  
              }
          }
          else //If we can't get data
          {
            if (debug) Serial.printf("[HTTP]Ora GET... failed, error: %s\n", http.errorToString(httpCode).c_str());
          }
          http.end();
        }
        else
        {
            if (debug) Serial.printf("[HTTP] Unable to connect \n");
        }
    }
  }
#endif
